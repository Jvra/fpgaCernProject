import argparse
import logging

parser = argparse.ArgumentParser(description='Replace dummy frames with DUMMY_XX in the given frame address list file.')

parser.add_argument('-f', '--file', help='Input frame address list file', required=True)

parser.add_argument('-o', '--output', help='Outfile where the converted file will be stored.', required=True)

args = parser.parse_args()

logging.basicConfig(level=logging.INFO,format='[%(levelname)s] (%(processName)-10s) %(message)s',)

original_file = open(args.file, 'r')
converted_file = open(args.output, 'w')

original_data = original_file.readlines()
original_data = [i.rstrip() for i in original_data]

original_file.close()

dummy_index = 0

# First frame is dummy
converted_file.write("DUMMY_%02d\n" % (dummy_index))

prev_row_address = int(("{0:032b}").format(int(original_data[1],16))[-22:-17], 2)

# Loop through all but the last 2 frames. Starting from 2nd frame
for frame in original_data[1:-2]:

	row_address = int(("{0:032b}").format(int(frame,16))[-22:-17], 2)

	if row_address != prev_row_address:
		dummy_index+=1
		converted_file.seek(converted_file.tell() - 9)
		converted_file.write("DUMMY_%02d\n" % (dummy_index))
		converted_file.write("DUMMY_%02d\n" % (dummy_index+1))
		prev_row_address = row_address
	else:
		converted_file.write(frame+'\n')

# Last 2 frames are dummy
converted_file.write("DUMMY_%02d\n" % (dummy_index))
converted_file.write("DUMMY_%02d\n" % (dummy_index+1))

converted_file.close()