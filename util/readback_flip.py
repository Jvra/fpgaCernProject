file = 'files_test_pl/readbackCapture-golden.rdbk'
output = 'pl_golden_flip.rdbk'

input_file = open(file, 'r')
data = input_file.readlines()
out = open(output, 'w')

for line in range(len(data)):
	for c in range(len(data[line])):
		if data[line][c] == '0':
			a = list(data[line])
			a[c] = '1'
			data[line] = "".join(a)
		elif data[line][c] == '1':
			a = list(data[line])
			a[c] = '0'
			data[line] = "".join(a)
	out.write(data[line])

out.close()
input_file.close()