import argparse
from projectTools.fileTools import merge_files

parser = argparse.ArgumentParser(description='Merge a list of files into a single file.')

parser.add_argument('-f', '--files', nargs='+', help='The list of files to merge', required=True)

parser.add_argument('-o', '--output', help='The outfile', required=True)

parser.add_argument('-d', '--delete', action='store_true', help='Delete the merged files', required=False, default=False)

parser.add_argument('-e', '--exclude', action='store_true', help='Exclude duplicate headers.\
	This option prevents the script from merging the first line of each file (header line) multiple times', required=False, default=False)

args = parser.parse_args()

merge_files(args.files, args.output, [], delete_old=args.delete, have_header=args.exclude)