from projectTools.fileTools import find_timeTag
from os import listdir
from os.path import isfile, join
import math
from collections import Counter

mypath = '/media/george/HDD_ntfs/test_pl/Frames'
iteration_size = 50
print_list = []

onlyfiles = [f for f in listdir(mypath) if isfile(join(mypath, f))]

f = []
for i in range(len(onlyfiles)):
	f.append(float(find_timeTag(onlyfiles[i])))
f = sorted(f)

l = []
rb_index = 0
iteration_index = 1
for i in range(len(f)-1):
	t1 = f[i+1]
	t2 = f[i]
	if rb_index == iteration_size-1:
		rb_index = -1
		print(f'Last readback on iteration {iteration_index} ' + str(f[i]))
		iteration_index += 1
	else:
		l.append(math.ceil(t1 - t2))
		if math.ceil(t1 - t2) in print_list:
			print(f'Iteration {iteration_index} breaks on {f[i]}')
	rb_index += 1

x = Counter(l)
print(sorted(x.items(), key=lambda p: p[0]))