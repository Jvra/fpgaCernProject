import pandas as pd

df = pd.read_csv("/home/george/Desktop/test_1_errors_cluster_4")
del df['id']
del df['date_time']
del df['is_angled']

df['time_tag2'] = df['time_tag']
cols = list(df)
cols.insert(1, cols.pop(cols.index('time_tag2')))
df = df.loc[:, cols]

df.replace("t", "True", inplace=True)
df.replace("f", "False", inplace=True)

df.to_csv("test_output.csv", index=False, header=True)
df = pd.read_csv("test_output.csv")

df["golden_bit_value"] = df["golden_bit_value"].astype(int)
df["masked_bit"] = df["masked_bit"].astype(int)
df["essential_bit"] = df["essential_bit"].astype(int)

df.to_csv("test_output.csv", index=False, header=False)