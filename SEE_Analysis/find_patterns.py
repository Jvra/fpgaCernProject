from itertools import combinations
from collections import OrderedDict
from collections.abc import Iterable
from projectTools.mbu_splitter import mbu_splitter
from projectTools.structuresTools import without_keys

import os
import operator
import csv
import argparse
import json
import logging


def flatten(l):
	for el in l:
		if isinstance(el, Iterable) and not isinstance(el, (str, bytes)):
			yield from flatten(el)
		else:
			yield el

def remove_file(filename):
	try:
		os.remove(filename)
	except OSError:
		pass

# Check if patterna has the same pattern as patternb
# Check only the shape (ignore only the frame address).
def isPattern_identical(patterna, patternb):

	if len(patterna) != len(patternb):
		return False

	patterna = [list(x) for x in patterna]
	patternb = [list(x) for x in patternb]

	for i in range(len(patterna)):
		patterna[i][4] = 0
		patternb[i][4] = 0

	return sorted(patterna) == sorted(patternb)

def groupby_frame_parity(paterns, analitical_group):

	list_keys = list(paterns.keys())
	for patterna in list_keys:
		# Check if patterna exists in patterns because the dictionary
		# changes dynamically inside the loop
		if patterna not in paterns:
			continue

		new_patterna = [list(x) for x in patterna]
		for x in range(len(new_patterna)):
			new_patterna[x][4] = 2
		new_patterna = tuple([tuple(x) for x in new_patterna])

		paterns[new_patterna] = paterns.pop(patterna)
		
		new_hash = hash(new_patterna)
		hasha = hash(patterna)
		hashb = hasha

		for patternb in list_keys:
			if patternb not in paterns:
				continue
			if patternb == patterna:
				continue

			if isPattern_identical(patterna, patternb):

				paterns[new_patterna] += paterns[patternb]
				del paterns[patternb]

				hashb = hash(patternb)
				break
				
		for d in analitical_group:
			if d['pattern'] == hasha or d['pattern'] == hashb:
				d.update({'pattern': new_hash})

	return (paterns, analitical_group)



def find_patterns(config, pattern_group, filtre):

	logging.basicConfig(level=logging.INFO,format='[%(levelname)s] (%(processName)-10s) %(message)s',)

	config_file = open(config, 'r')
	config_json = json.load(config_file)
	config_file.close()

	base_name					=	list(config_json['patterns_groups'][pattern_group].keys())[0]
	max_errors_per_time_tag		=	config_json['max_errors_per_timetag']
	min_valid_distance_count	=	config_json['patterns_groups'][pattern_group]['filtre'][filtre]['min_occurrences']
	max_errors_per_frame		=	config_json['max_errors_per_frame']
	output_folder				=	config_json['output_folder']
	input_file					=	csv.DictReader(open(os.path.join(config_json['output_path'], config_json['project_name'] + '_data.csv')))
	errors_per_time_tag 		=	{}
	accept_values				=	[]
	ignore_values				=	[]
	timetags_accept				=	config_json['patterns_groups'][pattern_group][base_name]
	timetags_accept				=	[timetags_accept[n:n+2] for n in range(0, len(timetags_accept), 2)]
	gvalue_indication			=	config_json['gvalue_indication']

	exclude_filtre_items = {'min_occurrences'}

	for k, v in without_keys(config_json['patterns_groups'][pattern_group]['filtre'][filtre],
		exclude_filtre_items).items():
		accept_values.append((k, v))

	for i in range(len(config_json['ignore_patterns'])):
		tmp_l = []
		for k, v in config_json['ignore_patterns'][i].items():
			tmp_l.append((k, v))
		ignore_values.append(tmp_l)
		del tmp_l

	extetion_name='.'+'.'.join([str(k)+'='+str(v) for k,v in accept_values]+['csv'])

	logging.info(f"Running pattern group {base_name} for {extetion_name}")

	output_folder = os.path.join(output_folder, base_name, '')
	os.makedirs(os.path.dirname(output_folder), exist_ok=True)

	def is_row_accepted(row,accept_values):
		d={}
		for k,v in accept_values:
			if k in d and d[k]:
				continue
			d[k]=(row[k]==v)
		return all(d[k] for k in d)

	def is_row_ignored(row, ignore_values):
		for i in ignore_values:
			if is_row_accepted(row, i):
				return True
		return False

	def is_timetag_accepted(row, timetags_accept):
		
		# If timetags_accept is empty then accept all timetags.
		if not timetags_accept:
			return True

		for timetag_range in timetags_accept:
			if timetag_range[0] <= float(row['time_tag']) <= timetag_range[1]:
				return True 
		return False

	def merge_patterns(group):
		lc = combinations(group, 2)
		for g in lc:
			if(g[0]==g[1]):
				continue
			if len(set(g[0]).intersection(g[1])) > 0:
				index0 = group.index(g[0])
				index1 = group.index(g[1])
				group[index0].extend(set(g[1]) - set(g[0]))
				del group[index1]
				return True
		return False


	"""
	Create dictionary test after filtering and exclusion.

	The format of the test dictionary is:

	test = {
		time_tag: {
			frame_index: [value, ...],
			...
		},
		...
	}

	Where value is
	((bit_frame, frame_address, golden_bit_value, masked_bit), row_info)

	Note:
		The alldata list is temporary and only used to store the loaded errors in '_loaded_data' file.

	"""
	test={}
	alldata=[]
	for row in input_file:

		key = row['time_tag']

		if not is_row_accepted(row,accept_values):
			continue

		if is_row_ignored(row, ignore_values):
			continue

		if not is_timetag_accepted(row, timetags_accept):
			continue

		key2 = int(row['frame_index'])
		if not key in test:
			test[key]={}

		if gvalue_indication:
			golden_bit_value = int(row['golden_bit_value'])
		# If gvalue_indication is 0 then make the golden_bit_value = 2.
		# This will make the script that visualizes the patterns to 
		# discard the golden_bit_value and to save the different types as the same image.
		else:
			golden_bit_value = 2

		value=((int(row['bit_frame']),int(row['frame_address'],16),golden_bit_value,int(row['masked_bit'])),row)

		if (key2 in test[key]):
			test[key][key2].append(value)
		else:
			test[key][key2]=[value]
		alldata.append(row)

	logging.info(f"	Generating _loaded_data file")
	if len(alldata) > 0:
		first_row = alldata[0]
		with open(output_folder+base_name+'_loaded_data'+extetion_name, 'w') as outfile:
			wr = csv.DictWriter(outfile, fieldnames=list(first_row))
			wr.writeheader()
			wr.writerows(alldata)
	else:
		logging.info(f"No error data for {extetion_name}")
		return
	del alldata


	"""
	Create the non_set_data_dict and errors_per_time_tag dictionaries using the test dictionary,
	if frame has less than max_errors_per_frame errors.

	The format of the non_set_data_dict and errors_per_time_tag dictionary is:

	non_set_data_dict = {
		(time_tag, frame_index, bit_frame, frame_address, golden_bit_value, masked_bit): row_info,
		...
	}

	errors_per_time_tag = {
		time_tag: [value, ...],
		...
	}

	where value is
	(frame_index, bit_frame, frame_address, golden_bit_value, masked_bit)

	"""
	non_set_data_dict={}
	non_set_data=[]
	set_data=[]
	for key in test:
		time_tag_errors = test[key]
		for frame_index_errors in time_tag_errors:
			if len(time_tag_errors[frame_index_errors])<=max_errors_per_frame:

				for v in time_tag_errors[frame_index_errors]:
					value=(frame_index_errors,)+v[0]
					non_set_data.append(v[1])
					non_set_data_dict[(key,)+value]=v[1]
					if key in errors_per_time_tag:
						errors_per_time_tag[key].append(value)
					else:
						errors_per_time_tag[key]=[value]
			else:
				for v in time_tag_errors[frame_index_errors]:
					set_data.append(v[1])
	del test

	logging.info(f"	Generating _non_sets file")
	if len(non_set_data)>0:
		first_row = non_set_data[0]
		with open(output_folder+base_name+'_non_sets'+extetion_name, 'w') as outfile:
			wr = csv.DictWriter(outfile, fieldnames=list(first_row))
			wr.writeheader()
			wr.writerows(non_set_data)
	else:
		remove_file(output_folder+base_name+'_non_sets'+extetion_name)
	del non_set_data

	logging.info(f"	Generating _sets file")
	if len(set_data)>0:
		first_row = set_data[0]
		with open(output_folder+base_name+'_sets'+extetion_name, 'w') as outfile:
			wr = csv.DictWriter(outfile, fieldnames=list(first_row))
			wr.writeheader()
			wr.writerows(set_data)
	else:
		remove_file(output_folder+base_name+'_sets'+extetion_name)
	del set_data


	"""
	Create final_distances dictionary using the errors_per_time_tag.

	The format of final_distances is:

	final_distances = {
		(frame_index0 - frame_index1, bit_frame0 - bit_frame1): count,
		...
	}

	The final_distances has the distances that appeared more than min_valid_distance_count.

	"""
	distances={}
	final_distances={}
	for time_tag in errors_per_time_tag:
		errors=errors_per_time_tag[time_tag]

		if len(errors)>max_errors_per_time_tag:
			continue

		lc = combinations(errors, 2)
		for item in lc:
			key = (item[0][0]-item[1][0],item[0][1]-item[1][1])
			"""
			This check is made in order to handle (u1 - u2) and (u1 - u3)
			and other patterns like that as the same patterns.

			    Frame
			B     | u2
			i --- u1 ---
			t  u3 |
			"""
			if key[0]*key[1]>=0:
				key= (abs(key[0]),abs(key[1]))
			elif key[0]<0:
				k=int(key[0]/abs(key[0]))
				key=(k*key[0],k*key[1])

			if key in distances:
				distances[key]+=1
			else:
				distances[key]=1

	sorted_distances = sorted(distances.items(), key=operator.itemgetter(1))

	logging.info(f"	Generating _distances file")
	with open(output_folder+base_name+'_distances'+extetion_name, 'w') as myfile:
		wr = csv.writer(myfile)
		for sd in sorted_distances[::-1]:
			wr.writerow(sd)

	for distance in distances:
		if distances[distance]>=min_valid_distance_count:
			final_distances[distance]=distances[distance]
	del distances

	sorted_distances = sorted(final_distances.items(), key=operator.itemgetter(1))

	logging.info(f"	Generating _distances_passed file")
	with open(output_folder+base_name+'_distances_passed'+extetion_name, 'w') as myfile:
		wr = csv.writer(myfile)
		for sd in sorted_distances[::-1]:
			wr.writerow(sd)

	del sorted_distances


	"""
	Creates the address_anomalies & points_w_d_per_time_tag structures using the errors_per_time_tag.

	The address_anomalies is only used to create the _address_anomalies file (see README.md for file description).

	The format of points_w_d_per_time_tag is:

	points_w_d_per_time_tag = {
		time_tag: {
			value1: [value2, ...],
			...
		},
		...
	}

	value is the same as in errors_per_time_tag.

	"""
	address_anomalies=[]
	errors_with_distances={}
	points_w_d_per_time_tag={}
	for time_tag in errors_per_time_tag:
		errors_with_distances={}
		errors=errors_per_time_tag[time_tag]

		if len(errors)>max_errors_per_time_tag:
			continue

		lc = combinations(errors, 2)
		for item in lc:
			key = (item[0][0]-item[1][0],item[0][1]-item[1][1])
			if key[0]*key[1]>=0:
				key= (abs(key[0]),abs(key[1]))
			elif key[0]<0:
				k=int(key[0]/abs(key[0]))
				key=(k*key[0],k*key[1])

			if key not in final_distances:
				continue

			if (item[0][0]-item[1][0]!=item[0][2]-item[1][2]):
				key=(time_tag,item[0],item[1])
				address_anomalies.append(key)

			if item[0] in errors_with_distances:
				errors_with_distances[item[0]].append(item[1])
			else:
				errors_with_distances[item[0]]=[item[1]]

			if item[1] in errors_with_distances:
				errors_with_distances[item[1]].append(item[0])
			else:
				errors_with_distances[item[1]]=[item[0]]

		points_w_d_per_time_tag[time_tag]=dict(errors_with_distances)

	logging.info(f"	Generating _address_anomalies file")
	with open(output_folder+base_name+'_address_anomalies'+extetion_name, 'w') as myfile:
		wr = csv.writer(myfile)
		for sd in address_anomalies:
			wr.writerow(sd)
	del address_anomalies

	# Here we create the mbu_errors structure by taking
	# the value1 of each group in points_w_d_per_time_tag and
	# the timetag.
	mbu_errors=[]
	for key in points_w_d_per_time_tag:
		t= points_w_d_per_time_tag[key]
		for k in t:
			mbu_errors.append((key,)+k)


	"""
	Store the errors in mbu_errors in file _mbus while the others in file _sbus.
	"""
	mbu_data=[]
	sbu_data=[]
	tmp = dict(non_set_data_dict)
	for mbu in mbu_errors:
		mbu_data.append(non_set_data_dict[mbu])
		del tmp[mbu]
	del mbu_errors
	for key in tmp:
		sbu_data.append(tmp[key])

	paterns ={}
	analitical_group=[]
	for time_tag in list(points_w_d_per_time_tag.keys()):
		errors=points_w_d_per_time_tag[time_tag]
		groups=[]

		for point in errors:

			"""
			If event point (the bit the event occured) exists in any group then add the errors to the first group
			that it will find without adding duplicates.

			For example:

			errors = {
				u2: [u3],
				u3: [u2, u4],
			}
			groups = [[u2,u3]]
			point = u3

			In this iteration point is u3 which exists in a group 0 of groups. So we add the other points u2,u4 to group 0
			but u2 already exists in group 0 so we only add u4.
			
			Else if any other error of u3 group exists in any group then add u3 to the first group that it will find.
			"""
			if any(point in group for group in groups):
				i,group = next((i,set(group)) for i,group in enumerate(groups) if point in group)
				err =set(errors[point])
				groups[i]+=list(err-group)
				continue
			if any(len(set(errors[point]).intersection(group))>0 for group in groups):
				i,group = next((i,set(group)) for i,group in enumerate(groups) if len(set(errors[point]).intersection(group))>0)
				groups[i]+=[point]
				continue

			group=[point]+errors[point]

			groups.append(group)

		while(merge_patterns(groups)):
			pass

		for group in groups:
			l = list(zip(*group))
			lx,ly=l[:2]
			minx=min(lx)
			miny=min(ly)
			patern = [(pnt[0]-minx,pnt[1]-miny,pnt[3],pnt[4],pnt[2] % 2) for pnt in group]
			patern = sorted(patern)
			for g in group:
				data=non_set_data_dict[(time_tag,)+g].copy()
				data['pattern']=hash(tuple(patern))
				data['pos_in_pattern']=(g[0]-minx,g[1]-miny)
				analitical_group.append(data)
			if tuple(patern) in paterns:
				paterns[tuple(patern)]+=1
			else:
				paterns[tuple(patern)]=1
			group = sorted(group)

	"""
	Split large MBU patterns into the minimum amount of smaller statistically more often to occur.
	The basic concept of the algorithm is to try to find the small MBUs that form the large one.

	For example, the most frequent patterns are:
       P1           P2              P3
    ┌───┬───┐    ┌───┬───┐      ┌───┬───┐
    |   | ● |    | ● | ● |      |   | ● |
    ├───┼───┤    └───┴───┘      ├───┼───┤
    | ● |   |  [(0,0), (1,0)]   | ● | ● |
    └───┴───┘                   └───┴───┘
  [(0,1), (1,0)]          [(1,0), (0,1), (1,1)]


	And we want to split the following pattern:
	[(3,0), (3,1), (2,1), (1,1), (0,2)]
	┌───┬───┬───┬───┐
	|   |   |   | ● |
	├───┼───┼───┼───┤
	|   | ● | ● | ● |
	├───┼───┼───┼───┤
	| ● |   |   |   |
	└───┴───┴───┴───┘

	The algorith shall split the above pattern into P1 (bottom left) and P3 (top rigth).
	"""
	if config_json['patterns_groups'][pattern_group]['split_enable']:
		for i in range(2, config_json['patterns_groups'][pattern_group]['split_num_of_events'] + 1):
			mbu_splitter(config_json['patterns_groups'][pattern_group],
				paterns, analitical_group, i, mbu_data, sbu_data)

	if len(mbu_data) > 0:
		first_row = mbu_data[0]
		logging.info(f"	Generating _mbus file")
		with open(output_folder+base_name+'_mbus'+extetion_name, 'w') as outfile:
			wr = csv.DictWriter(outfile, fieldnames=list(first_row))
			wr.writeheader()
			wr.writerows(mbu_data)
	else:
		remove_file(output_folder+base_name+'_mbus'+extetion_name)
	del mbu_data

	logging.info(f"	Generating _sbus file")
	first_row = sbu_data[0]
	with open(output_folder+base_name+'_sbus'+extetion_name, 'w') as outfile:
		wr = csv.DictWriter(outfile, fieldnames=list(first_row))
		wr.writeheader()
		wr.writerows(sbu_data)
	del sbu_data
	del errors_per_time_tag
	del final_distances

	# Group by frame address bit 0
	if not config_json['pframe_indication']:
		(paterns, analitical_group) = groupby_frame_parity(paterns, analitical_group)

	logging.info(f"	Generating _paterns file")
	sorted_paterns = sorted(paterns.items(), key=operator.itemgetter(1))
	with open(output_folder+base_name+'_paterns'+extetion_name, 'w') as myfile:
		wr = csv.writer(myfile)
		for sd in sorted_paterns[::-1]:
			wr.writerow([hash(sd[0]),sd[1]]+list(sd[0]))

	if len(analitical_group) > 0:
		first_row=analitical_group[0]
		logging.info(f"	Generating _groups file")
		with open(output_folder+base_name+'_groups'+extetion_name, 'w') as outfile:
			wr = csv.DictWriter(outfile, fieldnames=list(first_row))
			wr.writeheader()
			wr.writerows(analitical_group)
	else:
		remove_file(output_folder+base_name+'_groups'+extetion_name)
		logging.info(f"Could not form MBU patterns for {extetion_name}")


if __name__ == "__main__":

	parser = argparse.ArgumentParser(description='MBU patterns generation script.')

	parser.add_argument('-c', '--config', help='Input configuration json file', required=True)
	parser.add_argument('-f', '--filtre', help='Selects one filtre from the config file', required=True, 
		type=int, default=0)
	parser.add_argument('-g', '--pattern_group', help='Selects the pattern group from the config file', required=True, 
		type=int, default=0)

	args = parser.parse_args()

	find_patterns(args.config, args.pattern_group, args.filtre)