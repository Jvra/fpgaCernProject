from multiprocessing import Process,Manager
from projectTools.structuresTools import split_array_by_length
from projectTools.binaryStringTools import KeepOnlyBinary
from projectTools.maskTools import find_masked_frames,find_binary_masked_frames
from projectTools.non_essetial_frame_tools import find_non_essential_frames,find_binary_non_essential_frames
from projectTools.stopwatch import Stopwatch
from collections import OrderedDict
from projectTools.fileTools import get_file_paths,read_file,find_timeTag,is_readbackCapture,merge_files
from datetime import datetime, timedelta
import pytz

import argparse
import logging
import csv
import json
import os
import pandas as pd


#this should be moved to an other file and be imported

#       It takes an array (in this case an array of paths to readback files),
#   and splits it in multiple (processes_amount) equal arrays ,and then added to
#   an master array with length of processes_amount.
#       If the input array cant be divided equaly (len(rbdyfiles)%processes_amount!=0)
#   the excess paths will be splited
#   among the the at the output arrays
def split_work(rbdyfiles,processes_amount):
	number_of_files = len(rbdyfiles)
	rbdyfiles_per_thread=int(len(rbdyfiles)/processes_amount)
	thread_rbdyfiles=[]
	for i in range(0,number_of_files,rbdyfiles_per_thread):
		thread_rbdyfiles.append(rbdyfiles[i:i+rbdyfiles_per_thread])

	htrbdfs= thread_rbdyfiles[processes_amount:]
	heaping_thread_rbdyfiles=[]
	for htrbdf in htrbdfs:
		for f in htrbdf:
			heaping_thread_rbdyfiles.append(f)
	thread_rbdyfiles = thread_rbdyfiles[:processes_amount]
	index =0
	for file in heaping_thread_rbdyfiles:
		thread_rbdyfiles[index].append(file)
		index+=1
	return thread_rbdyfiles
def frame_address_information(frame_address,error_data):
	if(not ('DUMMY' in frame_address)):
		frame_address_bit = ("{0:032b}").format(int(frame_address,16))
		
		if block_type_range:
			block_type_bit = frame_address_bit[(-1)*(block_type_range[0]+1):(-1)*block_type_range[1] if block_type_range[1] else None]
			if(block_type_bit == '000'):
				block_type = 'CRAM'
			elif(block_type_bit == '001'):
				block_type = 'BRAM'
			else:
				block_type = str(block_type_bit)
		else:
			block_type = None

		if top_bottom_range:
			top_bot_bit = frame_address_bit[(-1)*(top_bottom_range[0]+1):(-1)*top_bottom_range[1] if top_bottom_range[1] else None]
			if(top_bot_bit == '0'):
				top_bot = 'Top'
			elif(top_bot_bit == '1'):
				top_bot = 'Bottom'
		else:
			top_bot = None
		
		if row_range:
			row_address = int(frame_address_bit[(-1)*(row_range[0]+1):(-1)*row_range[1] if row_range[1] else None],2)
		else:
			row_address = None

		if column_range:
			column_address = int(frame_address_bit[(-1)*(column_range[0]+1):(-1)*column_range[1] if column_range[1] else None],2)
		else:
			column_address = None

		if minor_range:
			minor_address = int(frame_address_bit[(-1)*(minor_range[0]+1):(-1)*minor_range[1] if minor_range[1] else None],2)
		else:
			minor_address = None
	else:
		block_type=None
		top_bot=None
		row_address = None
		column_address = None
		minor_address = None
	# return (block_type,top_bot,row_address,column_address,minor_address)
	error_data['frame_address']=frame_address
	error_data['block_type']=block_type
	error_data['top_bot']=top_bot
	error_data['row_address']=row_address
	error_data['column_address']=column_address
	error_data['minor_address']=minor_address
	return error_data

def is_masked_frame(frame_index):
	return masked_frames[frame_index]
def is_non_essential_frame(frame_index):
	if(frame_index<max_non_essential_frames):
		non_essential_frame = non_essential_frames[frame_index]
	else:
		non_essential_frame = True
	return non_essential_frame
def logic_data_info(frame_address,bit_pos_in_frame,error_data):
	logic_block=None
	specific_logic=None
	specific_logic_2=None
	SLR_name=None
	SLR_number=None

	try:
		logic_block=logic_data.at[(str(frame_address), bit_pos_in_frame), 'Block'].split('=')[1]
		specific_logic=logic_data.at[(str(frame_address), bit_pos_in_frame), 'Latch']
		specific_logic_2=logic_data.at[(str(frame_address), bit_pos_in_frame), 'Net']
		SLR_name=logic_data.at[(str(frame_address), bit_pos_in_frame), 'SLR_name']
		SLR_number=logic_data.at[(str(frame_address), bit_pos_in_frame), 'SLR_number']

	except KeyError as e:
		pass

	error_data['logic_block']		= logic_block if not pd.isna(logic_block) else None
	error_data['specific_logic']	= specific_logic if not pd.isna(specific_logic) else None
	error_data['specific_logic_2']	= specific_logic_2 if not pd.isna(specific_logic_2) else None
	error_data['SLR_name']			= SLR_name if not pd.isna(SLR_name) else None
	error_data['SLR_number']		= SLR_number if not pd.isna(SLR_number) else None
	return error_data

def find_Errors_in_Frame(frame_index,frame,golden_frame,frame_address,error_data,dict_writer):
	for bit_in_frame_index, bit in enumerate(frame):
		#if golden bit is equal to the bit that we checking
		#go to the next bit
		if (golden_frame[bit_in_frame_index]==bit):
			continue

		# calculating the intex of the error word relative to the frame
		word_intex_in_frame=bit_in_frame_index//word_bit_size

		# calculating the intex of the error bit relative to the word
		bit_intex_in_word = bit_in_frame_index -word_intex_in_frame*word_bit_size

		# POSsition is counting from the less to the most significant bit (rigth to left), deferent to index as intex counts from left to rigth
		# translating index to pos. to do the translation we subtract from the maximum intex value (word_bit_size-1=31) to the curent one (bit_in_frame_index)
		bit_pos_in_word = word_bit_size-1 -bit_intex_in_word

		# to find the possition of the bit with in the Index we need to translate the word_intex_in_frame to bits a.k.a. to multiply
		# by the amount of bits that create a word (word_bit_size) and the add up the possition of the bit within the word (bit_pos_in_word)
		bit_pos_in_frame = word_intex_in_frame*word_bit_size + bit_pos_in_word

		bit_index= frame_index*frame_bit_size+word_intex_in_frame*word_bit_size+bit_intex_in_word

		bit_pos=frame_index*frame_bit_size+word_intex_in_frame*word_bit_size+bit_pos_in_word

		golden_bit = golden_frame[bit_in_frame_index]

		masked_bit = int(mask_file_data[bit_index])


		essential_bit = 0
		if (bit_index<len(essential_bits)):
			essential_bit = int(essential_bits[bit_index])

		error_data=logic_data_info(frame_address,bit_pos_in_frame,error_data)

		error_data['golden_bit_value']=golden_bit
		error_data['word_of_frame']=word_intex_in_frame
		error_data['bit_word']=bit_pos_in_word
		error_data['bit_frame']=bit_pos_in_frame
		error_data['masked_bit']=masked_bit
		error_data['essential_bit']=essential_bit

		if any(error_data[k] in bits_to_ignore[k] for k in bits_to_ignore):
			continue
		dict_writer.writerow(error_data)
		current_set.add((frame_address, bit_pos_in_frame))

def find_errors_in_binary_frame(frame_index,frame,golden_frame,frame_address,error_data,dict_writer):
	for index,binary_int in enumerate(frame):
		g_binary_int=golden_frame[index]

		if(binary_int==g_binary_int):
			continue

		int_error = binary_int^g_binary_int

		byte_index=frame_index*frame_byte_size + index

		essential_byte = 0
		if (byte_index<len(essential_bits)):
			essential_byte = essential_bits[byte_index]

		word_intex_in_frame = index//word_byte_size
		byte_index_in_word = index- word_intex_in_frame*word_byte_size

		essential_byte = byteInt_to_binary_format.format(essential_byte)

		masked_byte =byteInt_to_binary_format.format(mask_file_data[byte_index])

		golden_byte =byteInt_to_binary_format.format(g_binary_int)

		binary_error=byteInt_to_binary_format.format(int_error)
		for i,bit in enumerate(binary_error):
			if(bit =='1'):

				bit_index_in_byte = i
				bit_intex_in_word = byte_index_in_word*byte_bit_size + bit_index_in_byte
				bit_pos_in_word = word_bit_size-1 -bit_intex_in_word
				bit_pos_in_frame = word_intex_in_frame*word_bit_size + bit_pos_in_word

				bit_pos=frame_index*frame_bit_size+word_intex_in_frame*word_bit_size+bit_pos_in_word
				golden_bit = golden_byte[i]

				masked_bit = int(masked_byte[i])
				essential_bit = int(essential_byte[i])

				error_data=logic_data_info(frame_address,bit_pos_in_frame,error_data)

				error_data['golden_bit_value']=golden_bit
				error_data['word_of_frame']=word_intex_in_frame
				error_data['bit_word']=bit_pos_in_word
				error_data['bit_frame']=bit_pos_in_frame
				error_data['masked_bit']=masked_bit
				error_data['essential_bit']=essential_bit
				if any(error_data[k] in bits_to_ignore[k] for k in bits_to_ignore):
					continue
				dict_writer.writerow(error_data)
				current_set.add((frame_address, bit_pos_in_frame))

def add_dummy_info(original_data):

	address_list = [i.rstrip() for i in original_data]

	if 'DUMMY' not in address_list[0]:

		dummy_index = 0

		# First frame is dummy
		address_list[0] = ("DUMMY_%02d" % (dummy_index), address_list[0])

		prev_row_address = int(("{0:032b}").format(int(address_list[1],16))[-22:-17], 2)

		# Loop through all but the last 2 frames. Starting from 2nd frame
		for frame_index, frame in enumerate(address_list[:-2]):

			if frame_index == 0:
				continue

			row_address = int(("{0:032b}").format(int(frame,16))[-22:-17], 2)

			if row_address != prev_row_address:
				dummy_index+=1
				address_list[frame_index-1] = ("DUMMY_%02d" % (dummy_index), address_list[frame_index-1])
				address_list[frame_index] = ("DUMMY_%02d" % (dummy_index+1), address_list[frame_index])
				prev_row_address = row_address

		# Last 2 frames are dummy
		address_list[-2] = ("DUMMY_%02d" % (dummy_index), address_list[-2])
		address_list[-1] = ("DUMMY_%02d" % (dummy_index+1), address_list[-1])
	else:

		address_list[0] = (address_list[0], "{:08x}".format(0))

		for frame_index, frame in enumerate(address_list[:-2]):

			if frame_index == 0:
				continue

			if 'DUMMY' in frame:
				if 'DUMMY' not in address_list[frame_index-1][0]:
					address_list[frame_index] = (address_list[frame_index], address_list[frame_index-1])
				elif 'DUMMY' not in address_list[frame_index+1]:
					address_list[frame_index] = (address_list[frame_index], address_list[frame_index+1])
				else:
					logging.error('Found more DUMMY frames than expected.')

		address_list[-2] = (address_list[-2], address_list[-3])
		address_list[-1] = (address_list[-1], "{:08x}".format(int(address_list[-3][1], 16) + 1))

	return address_list


def MainProcess(file_paths,results,process_intex):
	error_data=OrderedDict()

	global current_set
	global previous_set

	output_file = open(output_path+".p"+str(process_intex), 'w')
	debug_file = open(output_path+".debug.txt.p"+str(process_intex), 'w')
	same_bitflip_file = open(output_path+".same_bitflip.p"+str(process_intex), 'w')

	dict_writer = csv.DictWriter(output_file, output_keys)
	if(use_binary):
		find_errors_in_frame=find_errors_in_binary_frame
	else:
		find_errors_in_frame=find_Errors_in_Frame
	stopwatch = Stopwatch()
	stopwatch.Start()
	#result=[]

	counter = 0
	set_counter = 0
	prev_timetag = -1

	for file_index, file_path in enumerate(file_paths):

		logging.info(f'({file_index}/{len(file_paths)}) Parsing {file_path}')

		errors=[]

		current_set.clear()

		timeTag = find_timeTag(file_path)

		# Check whether we have a broken iteration
		if counter != 0 and float(timeTag) - float(prev_timetag) > readback_delay:
			counter = 0
			set_counter+=1
			previous_set.clear()

			debug_file.write('{}, {}, {}\n'.format(timeTag, str(readbackCapture), 'ITERATION_STOP'))
			debug_file.flush()

		prev_timetag = timeTag

		readback_iteration = counter
		test_iteration = set_counter

		if iteration_size > 1:
			# if the file is the 1st in the set , it will be compared with the golden_1
			if counter == 0:
				g_frames = golden1_frames
				counter+=1
				previous_set.clear()
			# if the its the 2nd it will be compared with the golden_2 but XORed with
			# the previous errors
			elif counter == 1 and golden2_exists:
				if use_binary:
					t = [bytearray([g1b^pb^g2b for g1b,pb,g2b in zip(g1f,pf,g2f)]) for g1f,pf,g2f in zip(golden1_frames,prev_frames,golden2_frames)]
				else:
					t = ["".join([str(int(g1b)^int(pb)^int(g2b)) for g1b,pb,g2b in zip(g1f,pf,g2f)]) for g1f,pf,g2f in zip(golden1_frames,prev_frames,golden2_frames)]
				g_frames= t
				counter+=1
			# else if it is the 3rd to 9nth it will be compared with the previous file
			elif counter < iteration_size-1:
				g_frames = prev_frames
				counter+=1
			# on last readback reset the iteration
			else :
				g_frames = prev_frames
				counter=0
				set_counter+=1
		else:
			g_frames = golden1_frames
			set_counter+=1

		if(use_binary):
			binary = read_file(file_path,'b', pipeline_words=pipeline_words)
			frames = split_array_by_length(binary,frame_byte_size)
		else:
			#reading the readback file:
			binary = read_file(file_path, pipeline_words=pipeline_words)
			#keeping only 0,1 characters (removing newline characters)
			binary = binary.translate(keepOnlyBinary)
			frames = split_array_by_length(binary,frame_bit_size)

		# If file is empty
		if not frames:
			logging.info('No data in %s, search lasted %f sec' % (file_path,stopwatch.ReStart()))
			debug_file.write('{}, {}, {}\n'.format(timeTag, str(readbackCapture), 'NO_DATA'))
			debug_file.flush()
			continue

		#coping the frames into the prev_frames to be used on the next readback file
		prev_frames=frames.copy()

		if g_frames == frames:
			logging.info('No Errors in %s, search lasted %f sec' % (file_path,stopwatch.ReStart()))
			debug_file.write('{}, {}, {}\n'.format(timeTag, str(readbackCapture), 'NO_ERROR'))
			debug_file.flush()
			continue

		error_data['time_tag']=timeTag
		error_data['date_time']=datetime.fromtimestamp(float(timeTag), tz)
		error_data['test_iteration']=test_iteration
		error_data['readback_iteration']=readback_iteration
		error_data['readback_capture']=readbackCapture
		for frame_index, frame in enumerate(frames):
			golden_frame=g_frames[frame_index]
			if(frame==golden_frame):
				continue
			error_data['frame_index']=frame_index
			frame_address = address_list[frame_index]

			if type(frame_address) == tuple:
				error_data['is_dummy'] = True
				frame_address = frame_address[1]
			else:
				error_data['is_dummy'] = False

			error_data=frame_address_information(frame_address,error_data)

			# Keep only errors of specified memory type
			if memory_type != 'BOTH':
				if error_data['block_type'] != memory_type:
					continue

			error_data['masked_frame']=is_masked_frame(frame_index)
			if error_data['block_type'] == 'BRAM':
				error_data['non_essential_frame'] = True
			else:
				error_data['non_essential_frame'] = is_non_essential_frame(frame_index)
			find_errors_in_frame(frame_index,frame,golden_frame,frame_address,error_data,dict_writer)

		for bitflip in current_set & previous_set:
			same_bitflip_file.write('{0}, {1[0]}, {1[1]}\n'.format(previous_timetag, bitflip))
			same_bitflip_file.write('{0}, {1[0]}, {1[1]}\n'.format(timeTag, bitflip))
			debug_file.flush()
		previous_set = current_set.copy()
		previous_timetag = timeTag

		#result.extend(errors)
		logging.info('finish search at %s lasted %f sec' % (file_path,stopwatch.ReStart()))
	#results[process_intex]=result

def find_errors(config, paths, readbackCapture_p, output_path_p):

	global output_path
	global output_keys
	global use_binary
	global current_set
	global previous_set
	global iteration_size
	global golden1_frames
	global golden2_frames
	global pipeline_words
	global keepOnlyBinary
	global frame_bit_size
	global readbackCapture
	global readback_delay
	global golden2_exists
	global tz
	global address_list
	global block_type_range
	global top_bottom_range
	global row_range
	global column_range
	global minor_range
	global memory_type
	global masked_frames
	global max_non_essential_frames
	global non_essential_frames
	global word_bit_size
	global mask_file_data
	global essential_bits
	global logic_data
	global bits_to_ignore

	output_path = output_path_p
	readbackCapture = readbackCapture_p

	output_keys=['time_tag', 'date_time', 'test_iteration', 'readback_iteration', 'readback_capture', 'golden_bit_value',
		'frame_address', 'is_dummy', 'frame_index', 'block_type', 'top_bot', 'row_address', 'column_address', 'minor_address',
		'word_of_frame', 'bit_word', 'bit_frame', 'masked_bit', 'masked_frame', 'essential_bit', 'non_essential_frame',
		'SLR_name', 'SLR_number', 'logic_block', 'specific_logic', 'specific_logic_2']

	#starting timer:
	#using timer to calculate performance
	stopwatch = Stopwatch()
	stopwatch.Start()

	#initializing this object:
	#   this object is use for the ascii parsing to keep only 0,1 caracters.
	#   i read the whole file and new line caracters are included
	keepOnlyBinary = KeepOnlyBinary()

	config_file = open(config, 'r')
	config_json = json.load(config_file)
	config_file.close()

	# initializing the log method
	logging.basicConfig(level=logging.INFO,format='[%(levelname)s] (%(processName)-10s) %(message)s',)

	previous_set=set()
	current_set=set()

	#		how many words a frame contains
	frame_word_size = config_json['frame_word_size']

	#		how many bits a word contains
	word_bit_size = config_json['word_bit_size']

	#		how many pipeline words exist in the device
	pipeline_words = config_json['pipeline_words']

	#		how many bits a byte contains
	byte_bit_size = 8

	#		how many bytes a word contains
	word_byte_size = word_bit_size//byte_bit_size

	#	how many bits a frame contains
	frame_bit_size = frame_word_size*word_bit_size

	#		how many bytes a frame contains
	frame_byte_size = frame_word_size*word_byte_size

	#		this is used to format an int into and standar length binary string
	byteInt_to_binary_format = "{0:0"+str(byte_bit_size)+"b}"

	#		list of files to ignore or to target
	files_to_ignore = config_json['files_to_ignore']
	target_files = config_json['target_files']

	#		list of frames to ignore
	frames_to_ignore={}

	#		list of bits to ignore
	bits_to_ignore={}

	#		timetag range to parse, List of tuples
	timetag_range_to_parse = config_json['timetag_range_to_parse']

	#		range of frames to ignore
	frames_range_to_ignore={}

	#		range of bits to ignore
	bits_range_to_ignore={}

	#		frame address description
	frad_description	= config_json['frame_address']
	block_type_range	= frad_description['block_type'] if 'block_type' in frad_description else None
	top_bottom_range	= frad_description['top_bottom'] if 'top_bottom' in frad_description else None
	row_range			= frad_description['row']		 if 'row' 		 in frad_description else None
	column_range		= frad_description['column']	 if 'column' 	 in frad_description else None
	minor_range			= frad_description['minor']		 if 'minor' 	 in frad_description else None

	#   if binary parsing is enabled '.bin' will be added to the values of
	#   golden_path, capture_golden_path, mask_file_path, essential_bits_path
	golden_path 		=	config_json['golden_path'] if not readbackCapture else ''
	capture_golden_path	=	config_json['capture_golden_path'] if readbackCapture else ''
	mask_file_path 		=	config_json['mask_file_path']
	essential_bits_path = 	config_json['essential_bits_path']
	address_list_path 	= 	config_json['address_list_path']
	ll_path 			=	config_json['ll_path']
	ll_header			=	config_json['ll_header']
	memory_type			=	config_json['memory_type']

	iteration_size		=	config_json['iteration_size']
	readback_delay		=	config_json['readback_delay'] if iteration_size > 1 else 0
	processes_amount	=	config_json['processes_amount'] if iteration_size == 1 else 1
	use_binary			=	config_json['use_binary']

	msd_ignore_lines	=	config_json['msd_ignore_lines'] if 'msd_ignore_lines' in config_json else 0
	ebd_ignore_lines	=	config_json['ebd_ignore_lines'] if 'ebd_ignore_lines' in config_json else 0
	ll_ignore_lines		=	config_json['ll_ignore_lines'] if 'll_ignore_lines' in config_json else 0

	tz = pytz.timezone(config_json['timezone'])

	if readbackCapture:
		golden2_exists = len(config_json['capture_golden_path']) == 2
	else:
		golden2_exists = len(config_json['golden_path']) == 2

	if(use_binary):
		golden_path = [s+'.bin' for s in golden_path]
		mask_file_path = mask_file_path+'.bin'
		capture_golden_path = [s+'.bin' for s in capture_golden_path] if readbackCapture else ['']
		essential_bits_path = essential_bits_path+'.bin'
		
	# Target only files_to_ignore
	if target_files:
		file_paths = [os.path.join(paths, i) for i in files_to_ignore if os.path.exists(os.path.join(paths, i))]
	# Remove files_to_ignore from file_paths
	else:
		file_paths = get_file_paths(paths)
		file_paths = [i for i in file_paths if os.path.basename(i) not in files_to_ignore]

	# Keep only timetag_range_to_parse from file_paths if list is not empty
	if timetag_range_to_parse:
		file_paths = [i for i in file_paths for r in timetag_range_to_parse if r[0]<=float(find_timeTag(i))<=r[1]]

	number_of_files = len(file_paths)

	# This needs to be done in order to parse the readbacks with the right order.
	if iteration_size > 1:
		file_paths = sorted(file_paths)

	logging.info('Parsing %d readback files'% number_of_files)

	if (number_of_files<processes_amount):
		processes_amount=number_of_files

	# Load .ll file to pandas dataframe
	logging.info('Loading Logic Allocation file, it migth take some time')
	converters = {'frame_address' : lambda x: x[2:]}
	dtypes = {'Block': 'string', 'Latch': 'string', 'Net': 'string', 'Ram': 'string', 'Rom': 'string',
		'offset': int, 'bit': 'string', 'frame_offset': int, 'SLR_number': 'int32', 'SLR_name': 'string'}
	logic_data = pd.read_csv(ll_path, sep='\s+', index_col=[2, 3], dtype=dtypes,
		skiprows=ll_ignore_lines, skipinitialspace=True,
		names=ll_header, converters=converters)
	logic_data = logic_data.drop(['bit', 'offset'], axis=1)

	splited_file_paths = split_work(file_paths,processes_amount)
	file = open(address_list_path,'r')
	address_list = file.readlines()
	file.close()

	# Add DUMMY information on address_list.
	address_list = add_dummy_info(address_list)

	if(use_binary):
		mask_file_data=read_file(mask_file_path,'b', pipeline_words=pipeline_words)
		masked_frames = find_binary_masked_frames(mask_file_data,frame_byte_size)

		if readbackCapture:
			capture_golden_binary = read_file(capture_golden_path[0],'b', pipeline_words=pipeline_words)
			golden1_frames = split_array_by_length(capture_golden_binary,frame_byte_size)
		else:
			golden_binary = read_file(golden_path[0],'b', pipeline_words=pipeline_words)
			golden1_frames = split_array_by_length(golden_binary,frame_byte_size)

		if iteration_size > 1 and golden2_exists:
			if readbackCapture:
				capture_golden_binary = read_file(capture_golden_path[1],'b', pipeline_words=pipeline_words)
				golden2_frames = split_array_by_length(capture_golden_binary,frame_byte_size)
			else:
				golden_binary = read_file(golden_path[1],'b', pipeline_words=pipeline_words)
				golden2_frames = split_array_by_length(golden_binary,frame_byte_size)

		essential_bits = read_file(essential_bits_path,'b', pipeline_words=pipeline_words)
		non_essential_frames = find_binary_non_essential_frames(essential_bits,frame_byte_size)

		mainProcess=MainProcess
	else:
		mask_file_data=read_file(mask_file_path, ignore_lines=msd_ignore_lines, pipeline_words=pipeline_words)
		mask_file_data = mask_file_data.translate(keepOnlyBinary)
		masked_frames = find_masked_frames(mask_file_data,frame_bit_size)

		if readbackCapture:
			capture_golden_binary = read_file(capture_golden_path[0], pipeline_words=pipeline_words)
			capture_golden_binary = capture_golden_binary.translate(keepOnlyBinary)
			golden1_frames = split_array_by_length(capture_golden_binary,frame_bit_size)
		else:
			golden_binary = read_file(golden_path[0], pipeline_words=pipeline_words)
			golden_binary = golden_binary.translate(keepOnlyBinary)
			golden1_frames = split_array_by_length(golden_binary,frame_bit_size)

		if iteration_size > 1 and golden2_exists:
			if readbackCapture:
				capture_golden_binary = read_file(capture_golden_path[1], pipeline_words=pipeline_words)
				capture_golden_binary = capture_golden_binary.translate(keepOnlyBinary)
				golden2_frames = split_array_by_length(capture_golden_binary,frame_bit_size)
			else:
				golden_binary = read_file(golden_path[1], pipeline_words=pipeline_words)
				golden_binary = golden_binary.translate(keepOnlyBinary)
				golden2_frames = split_array_by_length(golden_binary,frame_bit_size)

		essential_bits = read_file(essential_bits_path, ignore_lines=ebd_ignore_lines, pipeline_words=pipeline_words)
		essential_bits = essential_bits.translate(keepOnlyBinary)
		non_essential_frames =  find_non_essential_frames(essential_bits,frame_bit_size)

		mainProcess=MainProcess
	#initialize and start threads
	manager = Manager()
	return_dict = manager.dict()
	max_non_essential_frames= len(non_essential_frames)

	processes = []
	for i in range(processes_amount):
		processes.append(Process(target=mainProcess, args=[splited_file_paths[i],return_dict,i]))
		processes[i].start()
	#wait for every thread to finish

	for i in range(processes_amount):
		processes[i].join()

	logging.info('Merging output files')

	# Merge processes data files
	filenames = []
	for i in range(processes_amount):
		filenames.append(output_path+".p"+str(i))
	merge_files(filenames, output_path+'.csv', output_keys, delete_old=True, have_header=False)

	debug_files_path = os.path.join(config_json["output_path"], "debug")
	os.makedirs(os.path.dirname(debug_files_path), exist_ok=True)
	debug_files_path = os.path.join(debug_files_path, output_path)

	# Merge no errors files (if exists)
	filenames = []
	for i in range(processes_amount):
		f = output_path+'.debug.txt.p' + str(i)
		if os.path.exists(f):
			filenames.append(f)
	merge_files(filenames, debug_files_path + '_debug.csv', ['time_tag', 'readback_capture', 'debug_message'], delete_old=True, have_header=False)

	# Merge same_bitflip files (if exists)
	filenames = []
	for i in range(processes_amount):
		f = output_path+'.same_bitflip.p' + str(i)
		if os.path.exists(f):
			filenames.append(f)
	merge_files(filenames, debug_files_path + '_same_bitflip.csv', ['time_tag', 'frame_address', 'bit_frame'], delete_old=True, have_header=False)

	logging.info('total time: %f s'% (stopwatch.Elapsed()))

# globals
output_path = 0
output_keys = 0
use_binary = 0
previous_set = 0
current_set = 0
iteration_size = 0
golden1_frames = 0
golden2_frames = 0
pipeline_words = 0
keepOnlyBinary = 0
frame_bit_size = 0
readbackCapture = 0
readback_delay = 0
golden2_exists = 0
tz = 0
address_list = 0
block_type_range = 0
top_bottom_range = 0
row_range = 0
column_range = 0
minor_range = 0
memory_type = 0
masked_frames = 0
max_non_essential_frames = 0
non_essential_frames = 0
word_bit_size = 0
mask_file_data = 0
essential_bits = 0
logic_data = 0
bits_to_ignore = 0

if __name__ == "__main__":

	#initializing the argument parser:
	#   i use this object to have input in a user friendly way
	#   -h can be used to see all the options
	parser = argparse.ArgumentParser()

	parser.add_argument('-c', '--config', help='Input configuration json file', required=True)

	parser.add_argument('-a', '--capture', action='store_true', help='Input readback files are readback capture',
		required=False, default=False)

	parser.add_argument('-o', '--output', help='Output data file', required=True)

	parser.add_argument('paths', metavar='file', type=str,
		default=None, help='Folder of readback files')

	args = parser.parse_args()
	
	find_errors(args.config, args.paths, args.capture, args.output)