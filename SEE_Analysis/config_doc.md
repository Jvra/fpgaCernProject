# Configuration parameters

| Parameter             | Type         | Description   |
| --------------------- | ----         | ------------- |
| _comment              | String       | Ignore these values |
| frame_word_size       | Integer      | Number of words on each frame |
| word_bit_size         | Integer      | Number of bits on each word |
| frame_address         | Object       | The Frame Address Register Description. The format is Address Type: [to_bit,from_bit]. The Address Type is any of block_type, top_bottom, row, column, minor. |
| pipeline_words        | Integer      | Number of pipeline words |
| iteration_size        | Integer      | The number of readbacks each iteration cycle has |
| readback_delay        | Integer      | The number of seconds between two readbacks. This parameter is used to check if we have stopped the iteration before the end of the cycle |
| memory_type           | String       | Type of memory errors the .csv file will have. Values: CRAM, BRAM or BOTH |
| golden_path           | List(String) | List of golden readback files |
| capture_golden_path   | List(String) | List of golden readback capture files |
| mask_file_path        | String       | The .msd file generated from vivado |
| essential_bits_path   | String       | The .ebd file generated from vivado |
| ll_path               | String       | The .ll file generated from vivado |
| address_list_path     | String       | Frame address list file generated from external scripts |
| readback_folders      | List(Object) | The list of folders of readback files in format {"readback_capture|readback": "path"} |
| project_name          | String       | This name will be used from different scripts to store debug/output files. |
| output_path           | String       | The path used from the scripts to store output files. |
| files_to_ignore       | List(String) | List of files that will be ignored |
| target_files          | Bool         | Whether to ignore or to target the files_to_ignore files |
| timetag_range_to_parse| List(Tuple(Integer) | List of tuples of timetags that will be parsed. |
| timezone              | String       | The timezone used in output .csv file. Supported timezones can be found [here](https://gist.github.com/heyalexej/8bf688fd67d7199be4a1682b3eec7568)
| processes_amount      | Integer      | Number of processes the script will create |
| use_binary            | Bool         | An option that enables the use of binary readback files |
| msd_ignore_lines      | Integer      | Number of lines to ignore at the beginning of the .msd file |
| ebd_ignore_lines      | Integer      | Number of lines to ignore at the beginning of the .ebd file |
| ll_ignore_lines       | Integer      | Number of lines to ignore at the beginning of the .ll file |
| max_errors_per_timetag| Integer      | Maximum amount of errors per timetag to considered in MBU analysis. If some timetag has more than this number then it's errors will be ignored |
| min_occurrences       | Integer      | The minimum amount of occurrences an upset pair should have to considered in MBU analysis. Care should be taken for low value due to high memory consumption |
| max_errors_per_frame  | Integer      | The maximum amount of errors per frame to considered in MBU analysis |
| filtre                | List(Object) | A list of objects of "key: values" pairs which selects the error types. |
| patterns_groups       | List(Object) | The list of pattern groups objects. Each object contains the name of the group (must be first), the time tag range (optional) for that specific group and a **filtre**. |
| ignore_patterns       | List(Object) | These conditions will be used to ignore some errors on patterns generation |
| output_folder         | String       | The output folder for MBU analysis files |
| gvalue_indication     | Bool         | Enables or disables the indication of the golden value. |
| pframe_indication     | Bool         | Enables or disables the indication of parity frame value (odd/even). |
| split_selection       | Integer      | Specifies the max number of occurrences the MBUs that are going to be split should have. |
| split_occur_thresh    | Integer      | Specifies the min number of occurrences the MBUs that are used for the search should have. |
| split_num_of_events   | Integer      | Specifies the number of MBUs the large MBU is going to be splitted. |
| split_enable          | Bool         | Set True to run the split algorithm or False to disable it. |

*Note: Type notation Type1(Type2) can be read as Type1 of Type2. E.x. List(String) is List of Strings.*

<details>
<summary>Some common timezones</summary>
<ul>
    <li>CET</li>
    <li>CST6CDT</li>
    <li>EET</li>
    <li>EST</li>
    <li>EST5EDT</li>
    <li>GB</li>
    <li>GMT</li>
    <li>Greenwich</li>
    <li>UTC</li>
    <li>Universal</li>
    <li>UCT</li>
</ul>
</details>
<br/>

Because **find_errors** script has many parameters, only the important ones can be configured from the json file. The others like the **frames_to_ignore**, can be configured inside the script.