# Files

- **see_analysis.py**: This python file uses the below scripts and the external .json configuration file in order to automate the database & pattern images generation.
- **find_errors.py**: The main python script for database's .csv file generation. The following files are generated from this script.
    - debug: This file contains debug information about some readback files. It has 3 columns. The timetag of the file, whether its readback capture and the debug message (the reason of the entry). There are 3 types of debug messages.
        + NO_ERROR: The readback file has no erroneous frames.
        + ITERATION_STOP: The readback iteration stopped in this file. This may occure due to power off of the device.
        + NO_DATA: The readback file is empty.
    - same_bitflip: This file contains the cases that 2 sequentially readback files have an erroneous bit in the same frame address and bit position.
- **f_p.py**: This script generates the following files using the database .csv file. These files are generated for each case described in **patterns_groups** **filtre** parameter.
    - address_anomalies: This file should be empty. The entries of this file are the pair upsets tagged as "physically adjacent" that are on different rows or columns.
    - distances: The frequency of all pair upsets offsets.
    - distances_passed: The frequency of "physically adjacent" pair upset offsets (see [[1]](#1)). The selection is made based on **min_occurrences** parameter.
    - groups: The grouped errors with the additional information of pattern hash and their position in pattern. 
    - loaded_data: The final data on which the MBU analysis will be executed (after the exclusion of **ignore_patterns**).
    - mbus: The list of errors that tagged as MBUs.
    - sbus: The list of errors that tagged as SBUs.
    - non_sets: The list of errors that are on frames with less than or equal with **max_errors_per_frame** errors. 
    - sets: The list of errors that are on frames with more than **max_errors_per_frame** errors.
    - patterns: The patterns represented in text-like format. This file is used by the **visualize_patterns.py** for pattern images generation.
- **visualize_patterns.py**: This script uses the patterns files generated from **f_p.py** and creates the folder structure with the MBU images.
- **docx_generator.py**: This script generates a summary docx file of the MBU patterns. This document groups the minor addresses of MBU patterns.

# Database file & MBU pattern images description

## Database file

The database file is generated from **find_errors.py** script and has the following format.

| Field | Values | Description |
| ----- | ------ | ----------- |
| time_tag | XXXXXXXXXX.XXXXXX | The time_tag taken from the readback filename. |
| date_time | YYYY-MM-DD HH:MM:SS | The converted time_tag in human readable date time specified in **timezone** configuration parameter |
| test_iteration | Integer | The index of the readback iteration group. |
| readback_iteration | Integer | The index of the readback file in iteration group. |
| readback_capture | Bool | Whether the readback file was a capture or not. |
| golden_bit_value | 0 or 1 | The golden value of the error. |
| frame_address | 8 chars | The hexadecimal value of frame address. |
| is_dummy | Bool | Whether the erroneous frame is dummy or not. |
| frame_index | Integer | The index of the erroneous frame. |
| block_type | CRAM or BRAM or None | The block type value of frame address register specified in configuration json. |
| top_bot | Top or Bottom or None | The Top/Bottom value of frame address register specified in configuration json. |
| row_address | Integer or None | The row address value of frame address register specified in configuration json. |
| column_address | Integer or None | The column address value of frame address register specified in configuration json. |
| minor_address | Integer or None | The minor address value of frame address register specified in configuration json. |
| word_of_frame | Integer | The index of the erroneous word in frame. |
| bit_word | Integer | The index of the erroneous bit in word. |
| bit_frame | Integer | The index of the erroneous bit in frame. |
| masked_bit | 0 or 1 | Whether the erroneous bit is masked or not. |
| masked_frame | Bool | Whether the whole erroneous frame is masked. |
| essential_bit | 0 or 1 | Whether the erroneous bit is essential or not. |
| non_essential_frame | Bool | Whether the erroneous frame is non essential or not. |
| SLR_name | String or None | The SLR_name field taken from the .ll file |
| SLR_number | String or None | The SLR_number field taken from the .ll file |
| logic_block | String or None | The Block value taken from the .ll file |
| specific_logic | String or None | The Latch value taken from the .ll file |
| specific_logic_2 | String or None | The Net value taken from the .ll file |

## MBU pattern images

The MBU pattern images display the MBU pattern in a NxM grid. The column of the grid is the frame index and the row is the bit index. Also, the images have the following style:

1. Fill color: Red if golden bit is 0 else Blue.
2. Outline: Green outline if masked bit is 1 else no outline.

# How to use

1) First, we create a .json file and we specify the parameters as described in **config_doc.md**.

3) Then we run the following command.

        python3 see_analysis.py -c config.json

4) The generated files are located in outputs folder.

## References
<a id="1">[1]</a>
M. Wirthlin, D. Lee, G. Swift and H. Quinn, "A Method and Case Study on Identifying Physically Adjacent Multiple-Cell Upsets Using 28-nm, Interleaved and SECDED-Protected Arrays," in IEEE Transactions on Nuclear Science, vol. 61, no. 6, pp. 3080-3087, Dec. 2014, doi: 10.1109/TNS.2014.2366913.
