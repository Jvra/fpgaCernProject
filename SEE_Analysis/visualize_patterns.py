import re
import ast
import os
import json
import shutil
import argparse
import logging

from os import listdir
from os.path import isfile, join
from PIL import Image, ImageDraw, ImageFont
from projectTools.fileTools import get_patterns_folder_from_groups_file


def create_image(points, filename, folder):
	max_x = -1
	max_y = -1
	for i in points:
		if i[0] > max_x:
			max_x = i[0]
		if i[1] > max_y:
			max_y = i[1]
	im = Image.new('RGB', (max_x+1,max_y+1), "white")

	# Draw pattern with golden bit color
	for i in points:
		c = (255,0,0)				# default : red, when parameter gvalue_indication is 0
		if i[2] == 0: c = (255,0,0)	# 0 	  : red
		if i[2] == 1: c = (0,0,255) # 1 	  : blue
		im.putpixel((i[0], i[1]), c)

	# Scale image
	resize_v = 10
	im = im.resize(((max_x+1)*resize_v,(max_y+1)*resize_v), Image.BOX)
	im = im.resize((im.size[0] + 2,im.size[1] + 2))

	draw = ImageDraw.Draw(im)

	# Add grid
	for x in range(0, im.size[0], resize_v):
		draw.line([(x, 0), (x, im.size[1])], fill='black', width=2)

	for y in range(0, im.size[1], resize_v):
		draw.line([(0, y), (im.size[0], y)], fill='black', width=2)

	# Add Masked border
	for i in points:
		x = i[0] * resize_v
		y = i[1] * resize_v
		if i[3] == 1:
			draw.rectangle([(x+2, y+2), (x+resize_v-1,y+resize_v-1)], outline='green')

	# Add even/odd information on the filename of the image
	even_odd = ''
	p = points[0]
	if p[4] != 2:
		if p[0] + p[4] % 2 == 0:
			even_odd = '_even'
		else:
			even_odd = '_odd'

	# Save Image without overwrite
	num = 0
	save_file = folder+'/'+str(filename)+even_odd+'.png'
	while(isfile(save_file)):
		save_file = folder+"/"+str(filename)+'_'+str(num)+'.png'
		num+=1

	im.save(save_file, "PNG")

def clear_images_folder(folder):
	if os.path.isdir(folder):
		for f in listdir(folder):
			if(f != 'csv_files'):
				shutil.rmtree(folder+'/'+f)

def visualize_patterns(config, pattern_group):

	logging.basicConfig(level=logging.INFO,format='[%(levelname)s] (%(processName)-10s) %(message)s',)

	config_file = open(config, 'r')
	config_json = json.load(config_file)
	config_file.close()

	# Setup folders
	base_name			=	list(config_json['patterns_groups'][pattern_group].keys())[0]
	input_folder		=	os.path.join(config_json['output_folder'], base_name, '')
	patterns_folder		=	os.path.join(input_folder, 'patterns', '')
	csv_regex			=	base_name + '_paterns.*'

	# Delete old images
	clear_images_folder(patterns_folder)

	files = [f for f in listdir(input_folder)
		if isfile(join(input_folder,f)) and re.match(csv_regex, f)]

	for file in files:

		logging.info(f'Generating images for file {file}')

		folder = patterns_folder + get_patterns_folder_from_groups_file(file)

		if not os.path.exists(folder):
			os.makedirs(folder)

		with open(input_folder+file) as file:
			data = file.read().splitlines()
			for i in data:
				l = ast.literal_eval(i)
				hashh = l[0]
				freq = l[1]
				points = [ast.literal_eval(k) for k in list(l[2:])]
				create_image(points, f"{freq}_{hashh}", folder)

if __name__ == "__main__":

	parser = argparse.ArgumentParser(description='MBU images generation script.')

	parser.add_argument('-c', '--config', help='Input configuration json file', required=True)
	parser.add_argument('-g', '--pattern_group', help='Selects the pattern group from the config file', required=True, 
		type=int, default=0)

	args = parser.parse_args()

	visualize_patterns(args.config, args.pattern_group)