import re
import csv
import zipfile
import pathlib

from os import listdir, remove
from os.path import isfile, join, basename, dirname

def get_file_paths(folder):
	file_paths = [join(folder, f) for f in listdir(folder) if isfile(join(folder, f))]
	return file_paths

def read_file(file_path, mode='', ignore_lines=0, pipeline_words=0):
	if zipfile.is_zipfile(file_path):
		archive = zipfile.ZipFile(file_path, 'r')
		if not archive.namelist():
			return ''
		filename = archive.namelist()[0]
		data = archive.read(filename)
		if mode == 'b':
			data = data[pipeline_words*4:]
		else:
			data = data.decode('utf-8')
			data = "\n".join(data.splitlines()[ignore_lines+pipeline_words:])
			data += '\n'
		archive.close()
	else:
		file = open(file_path,'r'+mode)
		if mode == 'b':
			data = file.read()[pipeline_words*4:]
		else:
			data = "".join(file.readlines()[ignore_lines+pipeline_words:])
		file.close()
	return data

def find_timeTag(s):
	filename = basename(s)
	return re.findall('(\d+\.\d+)',filename)[-1]

def is_readbackCapture(s):
	filename = basename(s)
	return bool(re.match('.*readbackCapture.*',filename))

def merge_files(files_to_merge, output_file, header, delete_old=False, have_header=False):

	# If files to merge have header then take that header from the first file.
	if have_header:
		f = open(files_to_merge[0], 'r')
		header = f.readline().rstrip('\n').split(',')
		f.close()

	pathlib.Path(dirname(output_file)).mkdir(parents=True, exist_ok=True)

	with open(output_file, 'w') as outfile:

		writer = csv.writer(outfile)
		if header:
			writer.writerow(header)

		for fname in files_to_merge:
			with open(fname) as infile:
				if have_header:
					next(infile)
				for line in infile:
					outfile.write(line)
			if delete_old:
				remove(fname)

def get_patterns_folder_from_groups_file(file):
	filesplit = file.split('.')

	block_type_index = [i for i, word in enumerate(filesplit) if word.startswith('block_type=')]
	masked_bit_index = [i for i, word in enumerate(filesplit) if word.startswith('masked_bit=')]
	readback_index   = [i for i, word in enumerate(filesplit) if word.startswith('readback_capture=')]

	block_type_index = block_type_index[0] if len(block_type_index) else []
	masked_bit_index = masked_bit_index[0] if len(masked_bit_index) else []
	readback_index = readback_index[0] if len(readback_index) else []

	if block_type_index:
		block_type = filesplit[block_type_index].split('=')[1]

	if masked_bit_index:
		masked = '_Masked' if filesplit[masked_bit_index].split('=')[1] == '1' else '_Unmasked'
	else:
		masked = ''

	if readback_index:
		readback_folder = 'readback_capture/' if filesplit[readback_index].split('=')[1] == 'True' else 'readback/'
	else:
		readback_folder = ''

	block_folder = block_type + masked + '/'

	return readback_folder + block_folder