def split_array_by_length(array,length):
	return [ array[i:i+length] for i in range(0, len(array), length) ]#coped code

def without_keys(d, keys):
	return {x: d[x] for x in d if x not in keys}