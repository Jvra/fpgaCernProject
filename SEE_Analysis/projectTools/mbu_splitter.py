from projectTools.structuresTools import without_keys

import operator
import logging


# Returns True if pattern a exists in pattern b.
# Thus if all bits of a exists in b.
def MBUa_exists_in_MBUb(a, b):
	logging.debug('Check exists ' + str(a) + ' with ' + str(b))
	return len(b.intersection(a)) == len(a)

# Returns True if bit0 of frame address of pattern a
# is the same as pattern b.
def address_align(patterna, patternb):
	logging.debug('Check align ' + str(patterna) + ' ' + str(patternb))
	for i in patternb:
		if i[0] == patterna[0][0] and i[1] == patterna[0][1]:
			# i[4] contains the odd/even frame address information.
			if i[4] == patterna[0][4]:
				return True
			else:
				return False
	return False

# Updates the occurrences on paterns list and removes the
# MBU that got splitted
def update_patterns(paterns, mbu_to_split, splitted_events):
	# Update occurrences
	for used_MBU in splitted_events:
		for key in paterns:
			if used_MBU[0] == hash(key):
				paterns[key] += mbu_to_split[1]

	# Remove mbu_to_split
	for key in paterns:
		if hash(key) == hash(mbu_to_split[0]):
			del paterns[key]
			break # no need to keep the loop since each pattern is unique

# Update hash and pos_in_pattern values on analytical groups
def update_other_files(mbu_to_split_hash, splitted_events, analitical_group, mbu_data, sbu_data):
	for i in analitical_group:
		if i['pattern'] == mbu_to_split_hash:
			for s in splitted_events:
				if i['pos_in_pattern'] in s[1]:
					i['pattern'] = s[0]
					i['pos_in_pattern'] = tuple(map(operator.sub, i['pos_in_pattern'], s[2]))
					break # because i['pos_in_pattern'] has changed

	# Move sbu that used for the split into the _sbus file.
	for i in analitical_group:
		if i['pattern'] == mbu_to_split_hash:
			analitical_group.remove(i)
			mbu_data.remove(without_keys(i, {'pattern','pos_in_pattern'}))
			sbu_data.append(without_keys(i, {'pattern','pos_in_pattern'}))

splitted_events = []
split_num_of_events = 0

def split_mbu(patterns_groups_json, paterns, mbu_to_split, mbu_to_split_hash, number_of_splits):

	# If split_num_of_events exceed and there is more than one bit to split then return False
	if number_of_splits > split_num_of_events:
		return False
	elif number_of_splits == split_num_of_events and len(mbu_to_split[0]) != 0:
		return False
	elif number_of_splits <= split_num_of_events and len(mbu_to_split[0]) < 2:
		return True

	logging.debug('Pattern to split ' + str(mbu_to_split[0]) + str(mbu_to_split_hash))

	most_freq_mbus = sorted(paterns.items(),
		key=operator.itemgetter(1),
		reverse=True)

	max_x = max(mbu_to_split[0], key=operator.itemgetter(0))[0]
	max_y = max(mbu_to_split[0], key=operator.itemgetter(1))[1]

	x_y_mbu_to_split = set([(i[0], i[1]) for i in mbu_to_split[0]])

	for most_freq_mbu in most_freq_mbus:

		logging.debug('With ' + str(most_freq_mbu) + str(hash(most_freq_mbu[0])))

		original_most_freq_mbu = most_freq_mbu

		if most_freq_mbu[1] < patterns_groups_json['split_occur_thresh']:
			logging.debug("most_freq_mbu > split_occur_thresh")
			return False

		x_y_most_freq_mbu = set([(i[0], i[1]) for i in most_freq_mbu[0]])

		for x in range(max_x + 1):

			for y in range(max_y + 1):
				
				logging.debug('New position ' + str(x_y_most_freq_mbu))

				# If most_freq_mbu exists in mbu_to_split...
				if (MBUa_exists_in_MBUb(x_y_most_freq_mbu, x_y_mbu_to_split) and
				   address_align(most_freq_mbu[0], mbu_to_split[0])):

					logging.debug('Matched')
					
					number_of_splits = number_of_splits + 1

					splitted_events.append((hash(original_most_freq_mbu[0]), x_y_most_freq_mbu, (x,y)))

					before_mbu_to_split = mbu_to_split

					# remove the bits of most_freq_mbu from mbu_to_split
					for i in mbu_to_split[0]:
						if (i[0], i[1]) in x_y_most_freq_mbu:
							
							logging.debug('Found in ' + str(i))

							# Convert tuple of tuples in list of tuple in order to make the deletion.
							mbu_to_split = (list(mbu_to_split[0]), mbu_to_split[1])

							mbu_to_split[0].remove(i)

							# Convert back to tuple of tuples
							mbu_to_split = (tuple(mbu_to_split[0]), mbu_to_split[1])

					if split_mbu(patterns_groups_json, paterns, mbu_to_split, mbu_to_split_hash, number_of_splits):
						return True
					else:
						# Revert split
						number_of_splits = number_of_splits - 1
						splitted_events.pop()
						mbu_to_split = before_mbu_to_split

				x_y_most_freq_mbu = set([(i[0], i[1]+1) for i in list(x_y_most_freq_mbu)])
				most_freq_mbu = (tuple([(i[0], i[1]+1, i[2], i[3], i[4]) for i in list(most_freq_mbu[0])]), most_freq_mbu[1])

			x_y_most_freq_mbu = set([(i[0]+1, i[1]-(max_y+1)) for i in list(x_y_most_freq_mbu)])
			most_freq_mbu = (tuple([(i[0]+1, i[1]-(max_y+1), i[2], i[3], (i[4]+1) % 2) for i in list(most_freq_mbu[0])]), most_freq_mbu[1])


def mbu_splitter(patterns_groups_json, paterns, analitical_group, num_of_events, mbu_data, sbu_data):

	logging.info(f'Start mbu_splitter with num_of_events {num_of_events}')

	# If split_selection is zero mbu_splitter is disabled
	if not patterns_groups_json['split_selection']:
		logging.debug("split_selection is zero")
		return

	# If list is empty
	if (not paterns) or (not analitical_group):
		logging.debug("Patterns or analitical_group is empty")
		return

	global split_num_of_events
	split_num_of_events = num_of_events

	mbus_to_split = sorted(paterns.items(),
		key=operator.itemgetter(1))

	# Find the maximum number of bits the most frequent mbu have.
	most_freq_mbus = sorted(paterns.items(),
		key=operator.itemgetter(1),
		reverse=True)

	tmp_list = []
	for most_freq_mbu in most_freq_mbus:
		if most_freq_mbu[1] >= patterns_groups_json['split_occur_thresh']:
			tmp_list.append(most_freq_mbu[0])

	most_freq_mbu_max_size = len(max(tmp_list, key=len))

	for mbu_to_split in mbus_to_split:

		# The rest patterns will have occurences more than
		# 'split_selection' so return
		if mbu_to_split[1] > patterns_groups_json['split_selection']:
			logging.debug("mbu_to_split > split_selection")
			return

		if len(mbu_to_split[0]) > num_of_events * most_freq_mbu_max_size:
			continue

		logging.debug('Try to split ' + str(hash(mbu_to_split[0])))

		splitted_events.clear()

		if split_mbu(patterns_groups_json, paterns, mbu_to_split, hash(mbu_to_split[0]), 0):
			logging.debug("A split FOUND ")
			logging.debug("Splitted into")
			tmp = [item[0] for item in splitted_events]
			logging.debug(tmp)

			logging.debug('Splitted ' + str(hash(mbu_to_split[0])))

			update_patterns(paterns, mbu_to_split, splitted_events)
			update_other_files(hash(mbu_to_split[0]), splitted_events, analitical_group, mbu_data, sbu_data)

# This is for debug only,
# the script should not be
# called directly from terminal
if __name__ == "__main__":

	import json

	config_file = open('configs/test_psi2021/config_CRAM_UNMASKED_merged_PSI2021.json', 'r')
	config_json = json.load(config_file)
	config_file.close()

	logging.basicConfig(level=logging.INFO,format='[%(levelname)s] (%(processName)-10s) %(message)s',)

	mbu_splitter(config_json['patterns_groups'], paterns, analitical_group)