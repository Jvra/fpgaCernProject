import pandas as pd
import numpy as np
import ast
import subprocess
import argparse
import glob
import logging
import json
import os

from docx import Document
from docx.shared import Inches
from projectTools.fileTools import get_patterns_folder_from_groups_file

def get_minor_table(hashh, dashes, groups_csv):
	p1 = subprocess.Popen(['cat', groups_csv],
		stdout=subprocess.PIPE)
	p2 = subprocess.Popen(['grep', hashh], stdin=p1.stdout,
		stdout=subprocess.PIPE)
	p1.stdout.close()
	p3 = subprocess.Popen(['cut', '-d,', '-f14'], stdin=p2.stdout,
		stdout=subprocess.PIPE)
	p2.stdout.close()
	p4 = subprocess.Popen(['paste', '-sd', dashes+'\n'], stdin=p3.stdout,
		stdout=subprocess.PIPE)
	p3.stdout.close()
	p5 = subprocess.Popen(['sort', '-n'], stdin=p4.stdout,
		stdout=subprocess.PIPE)
	p4.stdout.close()
	p6 = subprocess.Popen(['uniq', '-c'], stdin=p5.stdout,
		stdout=subprocess.PIPE)
	p5.stdout.close()
	p7 = subprocess.Popen(['sort', '-nr'], stdin=p6.stdout,
		stdout=subprocess.PIPE)
	p6.stdout.close()

	output, err = p7.communicate()
	output = output.decode('utf-8').rstrip().split('\n')

	# print(output)

	table = []

	for i in output:

		data = i.split()
		# print(data)
		minors = data[1].split('-')

		table.append([data[0]] + minors)

	return table

def docx_generator(config):

	logging.basicConfig(level=logging.INFO,format='[%(levelname)s] (%(processName)-10s) %(message)s',)

	config_file = open(config, 'r')
	config_json = json.load(config_file)
	config_file.close()

	doc = Document()

	for pattern_group in config_json['patterns_groups']:

		# root_folder = "outputs/mbu_analysis/test_undervoltage_2_pl/"
		root_folder = os.path.join(config_json['output_folder'], list(pattern_group.keys())[0])

		doc.add_heading(list(pattern_group.keys())[0])

		# groups_csv = root_folder + "test_undervoltage_2_pl_groups.readback_capture=True.block_type=CRAM.masked_bit=1.csv"
		for groups_csv in glob.glob(os.path.join(root_folder, "*_groups*.csv")):

			file_minor = pd.read_csv(groups_csv)

			# patterns_folder = root_folder + "patterns/readback_capture/CRAM_Masked/"
			patterns_folder = os.path.join(root_folder, 'patterns', get_patterns_folder_from_groups_file(groups_csv))

			new_df = file_minor.groupby('pattern').agg({
				'minor_address': list,
				'pos_in_pattern': list
			}).reset_index()
			del file_minor

			doc.add_paragraph("Generated from files")
			doc.add_paragraph(groups_csv + "\n\n")

			data = {}
			occ = []

			for index, r in new_df.iterrows():

				# print()
				# print(r)

				uniq_pos = list(np.unique(list(map(lambda x: ast.literal_eval(x), r['pos_in_pattern'])), axis=0))
				uniq_pos = list(map(lambda x: tuple(x), uniq_pos))
				
				hashh = r['pattern']
				hashh = '\\'+str(hashh) if hashh < 0 else str(hashh)
				dashes = '-' * (len(uniq_pos) - 1)

				t = get_minor_table(hashh, dashes, groups_csv)
				data[str(r['pattern'])] = t
				occ.append([sum([int(i[0]) for i in t]), str(r['pattern']), uniq_pos])

			occ = sorted(occ, key=lambda x: x[0], reverse=True)

			for pattern in occ:

				h = pattern[1]
				freq = pattern[0]

				even_odd = ""

				if os.path.exists(f"{patterns_folder}{freq}_{h}_even.png"):
					even_odd = "_even"
				elif os.path.exists(f"{patterns_folder}{freq}_{h}_odd.png"):
					even_odd = "_odd"

				doc.add_picture(f"{patterns_folder}{freq}_{h}{even_odd}.png")

				doc.add_paragraph("Hash: " + h)
				doc.add_paragraph("Total occurrences: " + str(pattern[0]))

				table = doc.add_table(rows=1, cols=len(data[h][0]), style='Light Shading Accent 1')

				hdr = table.rows[0].cells
				hdr[0].text = 'occurrences'

				for i in range(1, len(data[h][0])):

					hdr[i].text = str(pattern[2][i-1])

				for j in range(len(data[h])):

					cells = table.add_row().cells

					for i in range(len(data[h][0])):

						cells[i].text = data[h][j][i]

				doc.add_paragraph()
				# break

	doc.save(os.path.join(config_json['output_path'], 'patterns.docx'))

if __name__ == "__main__":

	parser = argparse.ArgumentParser(description='Docx MBU patterns generation script.')

	parser.add_argument('-c', '--config', help='Input configuration json file', required=True)

	args = parser.parse_args()

	docx_generator(args.config)