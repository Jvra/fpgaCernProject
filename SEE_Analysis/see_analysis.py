import logging
import os
import shutil
import json
import argparse
import pytz

from find_errors import find_errors
from find_patterns import find_patterns
from visualize_patterns import visualize_patterns
from docx_generator import docx_generator

from projectTools.fileTools import merge_files


parser = argparse.ArgumentParser(description='SEE Analysis automation script.')

parser.add_argument('-c', '--config', help='Input configuration json file', required=True)

args = parser.parse_args()

logging.basicConfig(level=logging.INFO,format='[%(levelname)s] (%(processName)-10s) %(message)s',)


config_file = None
if os.path.isfile(args.config):
	config_file = open(args.config, 'r')
else:
	logging.error('Configuration file ' + args.config + ' does not exist.')
	exit()

json_obj = json.load(config_file)
config_file.close()

# === START AUTOMATIO SCRIPT ===

def check_config(config_name, obj=json_obj):
	
	if config_name not in obj:
		logging.error(f'Set {config_name} on configuration file.')
		exit()

def check_path(path):

	if not os.path.isfile(path):
		logging.error(f'Wrong {path} path.')
		exit()

def check_config_path(config_name):

	check_config(config_name)
	check_path(json_obj[config_name])

def check_config_all():
	
	check_config('frame_word_size')
	check_config('word_bit_size')
	check_config('frame_address')
	check_config('pipeline_words')

	check_config('readback_folders')

	if 'readback' in [list(i.keys())[0] for i in json_obj['readback_folders']]:
		check_config('golden_path')
		for i in json_obj['golden_path']:
			check_path(i)

	if 'readback_capture' in [list(i.keys())[0] for i in json_obj['readback_folders']]:
		check_config('capture_golden_path')
		for i in json_obj['capture_golden_path']:
			check_path(i)

	check_config_path('mask_file_path')
	check_config_path('essential_bits_path')
	check_config_path('ll_path')
	check_config_path('address_list_path')

	check_config('output_path')
	check_config('project_name')
	check_config('ll_header')

	check_config('memory_type')
	check_config('timezone')

	if json_obj['timezone'] not in pytz.all_timezones:
		logging.error(f"Unknown timezone {json_obj['timezone']}")
		exit()

	check_config('processes_amount')
	check_config('use_binary')

	check_config('max_errors_per_timetag')
	for i in json_obj['patterns_groups']:
		check_config('filtre', i)
		for j in i['filtre']:
			check_config('min_occurrences', j)
	check_config('max_errors_per_frame')	
	check_config('gvalue_indication')


check_config_all()

database_file = os.path.join(json_obj['output_path'], json_obj['project_name'] + '_data.csv')

# Run find errors script
if os.path.isfile(database_file):
	logging.info(f"Using previous generated database file {database_file}")
else:
	data_out_files = []
	no_errors_files = []
	for index, rb_path_json in enumerate(json_obj['readback_folders']):

		rb_path = list(rb_path_json.values())[0]
		rb_type = list(rb_path_json.keys())[0]

		logging.info(f"Running find errors script on folder {rb_path}")

		output_filename = f'{rb_type}_{str(index)}_out'

		if rb_type == 'readback_capture':
			find_errors(args.config, rb_path, True, output_filename)
		elif rb_type == 'readback':
			find_errors(args.config, rb_path, False, output_filename)
		else:
			logging.error(f'Unknown readback type {rb_type}. Ignore {rb_path} folder')
			continue

		data_out_files.append(output_filename + '.csv')

	# Merging files
	merge_files(data_out_files, database_file, [], delete_old=True, have_header=True)

# Run MBU patterns generation script
logging.info(f"Running MBU patterns generation script on {database_file} file")
for g in range(len(json_obj['patterns_groups'])):

	pattern_group = list(json_obj['patterns_groups'][g].keys())[0]
	output_folder = os.path.join(json_obj['output_folder'], pattern_group)

	if os.path.exists(output_folder):
		shutil.rmtree(output_folder)

	for i in range(len(json_obj['patterns_groups'][g]['filtre'])):
		find_patterns(args.config, g, i)

# Run MBU images generation script
logging.info(f"Running MBU images generation on {database_file}")
for g in range(len(json_obj['patterns_groups'])):
	visualize_patterns(args.config, g)

# Run docx MBU summary script
logging.info(f"Generating MBU summary docx")
docx_generator(args.config)
